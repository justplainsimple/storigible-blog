# Storigible Blog

This repository contains the files necessary to build and deploy 
the Storigible blog at [http://blog.storigible.com](http://blog.storigible.com).

## Architecture

This project is a blog, but composed as a static website for both 
speed, ease of deployment and maintenance.  No databases are 
required and nothing other than a web server that serves HTML is 
needed.  In addition, since the blog is being read over 99% of the 
time versus written, it makes no sense to have a complicated 
architecture.

This project is also separated from the main Storigible website 
and application because any traffic spikes to the blog (which does 
not make money) should not impact the performance of the 
application (which does make money).

## Requirements

* git
* ruby 1.9+
* ``bundler`` gem
* Linux or OSX operating system (for deployments)
* ``aws`` CLI tool
* ``AWS_ACCESS_KEY`` and ``AWS_SECRET_KEY`` stored as 
environment variables.

## Deployment

**Before you begin, ensure that your machine is capable of calling 
AWS using the CLI tool and that it has permission to update the 
``blog.storigible.com`` S3 bucket.**

Deployment couldn't be easier with the ``deploy.sh`` bash script.
Simply call it and the script will use the AWS CLI tool to sync 
the data in the ``build`` directory to S3.

