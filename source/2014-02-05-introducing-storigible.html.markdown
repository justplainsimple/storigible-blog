---
title: Introducing Storigible
date: 2014-02-05 04:10 UTC
tags:
---

Welcome to Storigible...well, sort of.  There actually isn't much to show of this new product right now except for this blog, but soon you will be able to discover all of its benefits.  I'm doing something fairly unique in business, allowing everyone (customers and all) to see a product be created, from the ground up, until it is an operational service.  It will be fully transparent product so that you always know what is going on.

The reason I'm building Storigible in this way is to prove to everyone that I mean it when I say I want this product to be transparent.  Now, transparent doesn't mean that I'm going to show every piece of financial and private data on this site, but transparent enough that everyone knows what went into this product and what they get out of it.  I believe that **transparency builds trust**, and people will pay for trust.

