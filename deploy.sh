#!/bin/bash

# Syncs to S3
# - uses RRS for cost-savings
# - deletes files on S3 that don't exist locally
aws s3 sync build/ s3://blog.storigible.com --storage-class=REDUCED_REDUNDANCY --delete

